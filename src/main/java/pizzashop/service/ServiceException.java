package pizzashop.service;

public class ServiceException extends Exception {
    ServiceException(String message) {
        super(message);
    }
}
