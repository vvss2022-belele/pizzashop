package pizzashop.service;

import pizzashop.model.Menu;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.List;
import java.util.stream.Collectors;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private final int MAX_TABLE = 8;
    private final int MIN_TABLE = 1;
    private final double MAX_AMOUNT = 100000000f;
    private final double MIN_AMOUNT = 0f;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo){
        this.menuRepo=menuRepo;
        this.payRepo=payRepo;
    }

    public List<Menu> getMenuData(){
        return menuRepo.getMenu().stream()
                .map(Menu::new)
                .collect(Collectors.toList());
    }

    public List<Payment> getPayments(){return payRepo.getAll(); }

    public void addPayment(int table, PaymentType type, double amount) throws ServiceException {
        Payment payment= new Payment(table, type, amount);

        if(table < MIN_TABLE || table > MAX_TABLE) {
            throw new ServiceException("Invalid table number");
        }

        if(amount < MIN_AMOUNT || amount > MAX_AMOUNT) {
            throw new ServiceException("Invalid amount");
        }

        payRepo.add(payment);
    }

    public double getTotalAmount(PaymentType type) throws ServiceException
    {
        double total=0.0f;
        List<Payment> l=getPayments();
        if ((l==null) ||(l.isEmpty()))
            return total;
        for (Payment p:l)
        {
            if (p.getType().equals(type))
            {
                if(p.getAmount() > 0)
                    total+=p.getAmount();
                else
                    throw new ServiceException("Negative amount");
            }
        }
        return total;
    }

}