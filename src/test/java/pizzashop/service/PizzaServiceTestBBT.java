package pizzashop.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.io.File;
import java.io.FileWriter;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test class for PizzaService")
@Timeout(2)
@TestMethodOrder(MethodOrderer.Random.class)
class PizzaServiceTestBBT {
    PizzaService service;
    final String PAYMENTS_FILE_PATH = "data/payments.txt";

    @BeforeEach
    void setUp() {
        try {
            File file = new File(PizzaServiceTestBBT.class.getClassLoader()
                    .getResource(PAYMENTS_FILE_PATH).getFile());
            new FileWriter(file).close();
        } catch (Exception ignored) {
        }

        MenuRepository repoMenu=new MenuRepository();
        PaymentRepository payRepo= new PaymentRepository();
        service = new PizzaService(repoMenu, payRepo);
    }

    @AfterEach
    void tearDown() {
    }

    // TC1_ECP
    @Tag("ECP")
    @Test
    void addPayment_validTable_success_ECP() {
        Payment payment = new Payment(2, PaymentType.Card, 123.1),
            resultPayment = null;

        try {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        resultPayment = service.getPayments().get(0);
        assertEquals(payment, resultPayment);
    }

    // TC2_ECP, TC3_ECP
    @Tag("ECP")
    @ParameterizedTest
    @ValueSource(ints = {-3, 12})
    void addPayment_invalidTable_exception_ECP(int table) {
        ServiceException thrown = Assertions.assertThrows(ServiceException.class, () -> {
            service.addPayment(table, PaymentType.Card, 321.2);
        });

        assertEquals("Invalid table number", thrown.getMessage());
    }

    // TC4_ECP
    @Tag("ECP")
    @Test
    void addPayment_negativeAmount_exception_ECP() {
        ServiceException thrown = Assertions.assertThrows(ServiceException.class, () -> {
            service.addPayment(3, PaymentType.Card, -0.5);
        });

        assertEquals("Invalid amount", thrown.getMessage());
    }

    // TC1_BVA, TC6_BVA
    @Tag("BVA")
    @ParameterizedTest
    @ValueSource(ints = {0, 9})
    void addPayment_invalidTable_exception_BVA(int table) {
        ServiceException thrown = Assertions.assertThrows(ServiceException.class, () -> {
            service.addPayment(table, PaymentType.Card, 123);
        });

        assertEquals("Invalid table number", thrown.getMessage());
    }

    // TC2_BVA
    @Tag("BVA")
    @Test
    void addPayment_tableLowerLimit_success_BVA() {
        Payment payment = new Payment(1, PaymentType.Card, 123),
                resultPayment = null;

        try {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        resultPayment = service.getPayments().get(0);
        assertEquals(payment, resultPayment);
    }

    // TC8_BVA
    @Tag("BVA")
    @Test
    void addPayment_amountLowerLimit_success_BVA() {
        Payment payment = new Payment(5, PaymentType.Card, 0),
                resultPayment = null;

        try {
            service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        resultPayment = service.getPayments().get(0);
        assertEquals(payment, resultPayment);
    }
}