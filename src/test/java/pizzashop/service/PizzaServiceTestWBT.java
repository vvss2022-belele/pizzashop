package pizzashop.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.io.File;
import java.io.FileWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Test class for PizzaService")
@Timeout(2)
class PizzaServiceTestWBT {
    PizzaService service;
    PaymentRepository repo;
    final String PAYMENTS_FILE_PATH = "data/payments.txt";

    @BeforeEach
    void setUp() {
        try {
            File file = new File(PizzaServiceTestWBT.class.getClassLoader()
                    .getResource(PAYMENTS_FILE_PATH).getFile());
            new FileWriter(file).close();
        } catch (Exception ignored) {
        }

        MenuRepository repoMenu=new MenuRepository();
        repo= new PaymentRepository();
        service = new PizzaService(repoMenu, repo);
    }

    @AfterEach
    void tearDown() {
    }

    // F02_TC01
    @Test
    void getTotalAmount_null_list() {
        repo.paymentList = null;

        try {
            assertEquals(0, service.getTotalAmount(PaymentType.Cash));
        } catch (ServiceException e) {
            assert(false);
        }
    }

    // F02_TC03
    @Test
    void getTotalAmount_negative_amount() {
        try {
            File file = new File(PizzaServiceTestWBT.class.getClassLoader()
                    .getResource(PAYMENTS_FILE_PATH).getFile());

            FileWriter writer = new FileWriter(file);

            writer.write("1,Cash,-10");

            writer.close();
            repo.readPayments();

            ServiceException thrown = Assertions.assertThrows(ServiceException.class, () -> {
                service.getTotalAmount(PaymentType.Cash);
            });

            assertEquals("Negative amount", thrown.getMessage());
        } catch (Exception ignored) {
            assert(false);
        }
    }

    // F02_TC04
    @Test
    void getTotalAmount_valid_payment() {
        try {
            File file = new File(PizzaServiceTestWBT.class.getClassLoader()
                    .getResource(PAYMENTS_FILE_PATH).getFile());

            FileWriter writer = new FileWriter(file);

            writer.write("1,Cash,50");

            writer.close();
            repo.readPayments();

            assertEquals(50, service.getTotalAmount(PaymentType.Cash));
        } catch (Exception ignored) {
            assert(false);
        }
    }

    // F02_TC05
    @Test
    void getTotalAmount_different_type() {
        try {
            File file = new File(PizzaServiceTestWBT.class.getClassLoader()
                    .getResource(PAYMENTS_FILE_PATH).getFile());

            FileWriter writer = new FileWriter(file);

            writer.write("1,Cash,40");

            writer.close();
            repo.readPayments();

            assertEquals(0, service.getTotalAmount(PaymentType.Card));
        } catch (Exception ignored) {
            assert(false);
        }
    }
}